export default {
    newEvent: 'new event',
    confirm: 'ok',
    cancel: 'cancel',
    eventName: 'event name',
    description: 'description',
    noEvents: 'no events',
    selectTime: 'select time',
}