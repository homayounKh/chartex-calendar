import faTranslate from "./fa-translate.js";
import enTranslate from "./en-translate.js";
import { createI18n } from 'vue-i18n'

const i18n = createI18n({
    locale: 'fa',
    legacy: false,
    messages: {
        'fa': faTranslate,
        'en': enTranslate
    }
})

export default i18n;