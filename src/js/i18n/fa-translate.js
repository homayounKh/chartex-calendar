export default {
    newEvent: 'رویداد جدید',
    confirm: 'تایید',
    cancel: 'لغو',
    eventName: 'نام رویداد',
    description: 'توضیحات',
    noEvents: 'رویدادی وجود ندارد',
    selectTime: 'انتخاب زمان',
}