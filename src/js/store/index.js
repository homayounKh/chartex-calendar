import {defineStore} from "pinia";
import moment from "moment-jalaali";

export const useAppStore = defineStore('app', {
    state: () => ({
        events: {},
        eventId: 0
    }),
    getters: {
        getEventsByDay(state) {
            return (day) => {
                console.log()
                if(!state.events[day])
                    return [];
                return state.events[day];
            }
        }
    },
    actions: {
        addDay(day) {
            const eventsKey = Object.keys(this.events);
            if(eventsKey.length === 0 || !eventsKey.includes(day)) {
                this.events[day] = [];
            }
        },
        addEvent(day, data) {
            const eventsKey = Object.keys(this.events);
            data.id = ++this.eventId
            if(eventsKey.includes(day)) {
                this.events[day].push({...data});
            }

            this.sortEvents(day)
        },
        sortEvents(day) {
            this.events[day].sort((ev1, ev2) => {
                return ev1.time >= ev2.time ? 1 : -1
            })
        },
        editEvent(day, event) {
            this.events[day] = this.events[day].map(item => {
                if(item.id === event.id)
                    return {...event}
                return item
            });
        }
    },

})