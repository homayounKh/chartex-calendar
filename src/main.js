import { createApp } from 'vue'
import App from './App.vue'
import i18n from "./js/i18n/index.js";
import {createPinia} from "pinia";

import './css/style.scss'

createApp(App)
    .use(i18n)
    .use(createPinia())
    .mount('#app')
